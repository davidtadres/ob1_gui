"""
The point of this GUI is to have an easy interface to the OB1
controller.
It became necessary as I needed to be able to keep a low pressure on
the channels after an initial purge.
By having an infinite loop through this GUI I plan to use this GUI to
load stimulation files, run a given stimulus and then go back to
keeping everything at the desired pressure.


To create the purge protocol I used the following commands:
purge_protocol = {}
purge_protocol['Channel 1'] = '10s@200mbar', '10mbar'
purge_protocol['Channel 2'] = '10s@200mbar', '10mbar'
purge_protocol['Channel 3'] = '10s@200mbar', '10mbar'
purge_protocol['Channel 4'] = '10s@200mbar', '10mbar'

with open(Path(Path.cwd(), 'Epyhs', 'control_OB1', 'purge_files', 'purge_file.json'),'w') as file:
    json.dump(purge_protocol, file, sort_keys=True, indent=4)
"""

import json
import tkinter as tk
from pathlib import Path
from tkinter import filedialog
import matplotlib.backends.backend_tkagg as tkagg
from matplotlib.figure import Figure
import numpy as np
import pandas as pd
import ctypes
import Elveflow64 as ESI
import datetime
import time

FREQUENCY = 50 # Hz
MIN_PRESS = 0
MAX_PRESS12 = 200 # This is for safety: Our OB1 can only deliver 200mbar in CH1 and CH2.
MAX_PRESS34 = 2000

####################
# OB1 codes - define here to have them as global variables.
#
# Initialization of OB1 ( ! ! ! REMEMBER TO USE .encode('ascii') ! ! ! )
#
Instr_ID=ctypes.c_int32()
print("Instrument name and regulator types are hardcoded in the Python script")
#see User Guide to determine regulator types and NIMAX to determine the instrument name
error=ESI.OB1_Initialization('0175C507'.encode('ascii'),1,1,2,2,
                          ctypes.byref(Instr_ID))
#all functions will return error codes to help you to debug your code, for further information refer to User Guide
print('error:%d' % error)
print("OB1 ID: %d" % Instr_ID.value)

####################
# Calibration
Calib = (ctypes.c_double * 1000)()  # always define array this way, calibration should have 1000 elements

Calib_path = str(Path(Path.cwd(), 'Calib.txt'))
error = ESI.Elveflow_Calibration_Load(Calib_path.encode(
    'ascii'), ctypes.byref(Calib), 1000)
####################

class OB1_GUI(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        tk.Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent
        self.parent.wm_title('Odor stimulation')

        self.purge_frame = tk.LabelFrame(self.parent)
        self.purge_frame.grid(row=0, column=0)

        self.stimulation_frame = tk.LabelFrame(self.parent)
        self.stimulation_frame.grid(row=1, column=0)

        self.stim_start_frame = tk.LabelFrame(self.parent)
        self.stim_start_frame.grid(row=2, column=0)

        self.current_pressure_frame = tk.LabelFrame(self.parent)
        self.current_pressure_frame.grid(row=3, column=0)

        self.purge_file_path = None
        self.purge_file = None
        self.purge_text = tk.StringVar()
        self.purge_text.set('\n\n\n\n')

        self.purge_channels = []
        self.purge_initial_time = []
        self.purge_initial_pressure = []
        self.purge_continuous_pressure = []

        self.stimulation_file = None
        self.stim_file_path = None
        self.stim_file_name = tk.StringVar()

        self.Ch1_display_var = tk.StringVar()
        self.Ch2_display_var = tk.StringVar()
        self.Ch3_display_var = tk.StringVar()
        self.Ch4_display_var = tk.StringVar()

        self.save_directory = tk.StringVar()
        self.save_directory.set(Path('D:/'))

        self.stim_duration = None

        self.purge_initiated = False

        self.odor_var = tk.StringVar()

        #################################################################
        # PURGE
        load_purge_button = tk.Button(self.purge_frame,
                                      text='Load Purge Protocol',
                                      command=self.load_purge_func)
        load_purge_button.grid(row=0, column=0)

        loaded_purge_file_label = tk.Label(self.purge_frame,
                                               textvar=self.purge_text)
        loaded_purge_file_label.grid(row=0, column=1)

        initiate_purge_button = tk.Button(self.purge_frame,
                                          text='Initate Purge',
                                          command=self.initate_purge_func)
        initiate_purge_button.grid(row=0, column=2)

        #################################################################
        # STIM
        load_stim_button = tk.Button(self.stimulation_frame, text='Load stim File',
                                command=self.load_func)
        load_stim_button.grid(row=0, column=0)

        loaded_stim_file_label = tk.Label(self.stimulation_frame,
                                          textvar=self.stim_file_name)
        loaded_stim_file_label.grid(row=1, column=0)

        # Plot to display the currently loaded stimulation
        self.fig = Figure(figsize=(5,3))
        self.ax = self.fig.add_subplot(111)

        self.frame_stim_plot = tk.Frame(self.stimulation_frame)
        self.frame_stim_plot.grid(row=2, column=0)
        self.canvas = tkagg.FigureCanvasTkAgg(
            self.fig, master=self.frame_stim_plot)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack()

        #################################################################
        # Start stim and where to save the stimulus data

        select_save_button = tk.Button(self.stim_start_frame,
                                   text='Save to...',
                                   command=self.select_save_folder_func)
        select_save_button.grid(row=0, column=0)

        save_label = tk.Label(self.stim_start_frame,
                              textvar = self.save_directory)
        save_label.grid(row=0, column=1)

        odor_label = tk.Label(self.stim_start_frame,
                              text='Presented Odor')
        odor_label.grid(row=1, column=0)

        odor_entry = tk.Entry(self.stim_start_frame,
                              textvariable=self.odor_var)
        odor_entry.grid(row=1, column=1)

        start_stimulus = tk.Button(self.stim_start_frame,
                                   text='START STIMULUS',
                                   command=self.present_stimulus_func)
        start_stimulus.grid(row=2, column=0, columnspan = 1)

        #################################################################
        # Display current values
        current_value_label = tk.Label(self.current_pressure_frame,
                                       text='Current pressure values')
        current_value_label.grid(row=0, column=0, columnspan = 4)
        ch1_label = tk.Label(self.current_pressure_frame, textvar=self.Ch1_display_var)
        ch1_label.grid(row=1, column=0)

        ch2_label = tk.Label(self.current_pressure_frame, textvar=self.Ch2_display_var)
        ch2_label.grid(row=1, column=1)

        ch3_label = tk.Label(self.current_pressure_frame, textvar=self.Ch3_display_var)
        ch3_label.grid(row=1, column=2)

        ch4_label = tk.Label(self.current_pressure_frame, textvar=self.Ch4_display_var)
        ch4_label.grid(row=1, column=3)

        #################################################################
        # QUIT
        quit_button = tk.Button(self.parent, text='Quit',
                                command=self.quit_func)
        quit_button.grid(row=4, column=1)

        #test_button = tk.Button(self.parent, text='Test',
        #                command=self.test_func)
        #test_button.grid(row=0, column=0)

        # automatically load purge protocol as this will almost never
        # change
        self.purge_file_path = Path(Path.cwd(), 'purge_files',
                                    'purge_file.json')
        with open(self.purge_file_path, 'r') as file:
            self.purge_file = json.load(file)
        self.purge_text.set(
            'Ch1: ' + str(self.purge_file['Channel 1']) +
            '\nCh2: ' + str(self.purge_file['Channel 2']) +
            '\nCh3: ' + str(self.purge_file['Channel 3']) +
            '\nCh4: ' + str(self.purge_file['Channel 4'])
        )

        self.callback_function()

    def callback_function(self):
        """
        This function is called every 1000ms
        :return:
        """
        self.after(1000, self.callback_function)
        self.Ch1_display_var.set('Ch1 [mbar]\n' + repr(round(self.read_OB1_pressure(1),1)))
        self.Ch2_display_var.set('Ch2 [mbar]\n' + repr(round(self.read_OB1_pressure(2),1)))
        self.Ch3_display_var.set('Ch3 [mbar]\n' + repr(round(self.read_OB1_pressure(3),1)))
        self.Ch4_display_var.set('Ch4 [mbar]\n' + repr(round(self.read_OB1_pressure(4),1)))

    def select_save_folder_func(self):
        self.save_directory.set(Path(filedialog.askdirectory(title='Select Save Folder',
                                               initialdir='D:/')))

    def load_purge_func(self):
        self.purge_file_path = filedialog.askopenfile(
                                            initialdir=Path.cwd()) #
        # fix initialdir to folder once on the ephys PC
        with open(Path(self.purge_file_path.name), 'r') as file:
            self.purge_file = json.load(file)
        for channel in self.purge_file:
            print(channel)

        self.purge_text.set(
            'Ch1: ' + str(self.purge_file['Channel 1']) +
            '\nCh2: ' + str(self.purge_file['Channel 2']) +
            '\nCh3: ' + str(self.purge_file['Channel 3']) +
            '\nCh4: ' + str(self.purge_file['Channel 4'])
        )

    def initate_purge_func(self):
        # Probably have to do this while at the ephys PC...
        print('purging')

        # Need to re-initialize otherwise would just append if
        # re-loading a new purge file!
        self.purge_channels = []
        self.purge_initial_time = []
        self.purge_initial_pressure = []
        self.purge_continuous_pressure = []

        for i_channel in self.purge_file:
            self.purge_channels.append(i_channel)
            self.purge_initial_time.append(
                float(self.purge_file[i_channel][0].split('s')[0]))
            self.purge_initial_pressure.append(
                float((self.purge_file[i_channel][0].split('@')[
                    1]).split('mbar')[0]))
            self.purge_continuous_pressure.append(
                float(self.purge_file[i_channel][1].split('mbar')[0])
            )
        # Check if initial purge time is identical and let user know
        # if that's not the case!
        if self.purge_initial_time.count(self.purge_initial_time[0]) \
                == len(self.purge_initial_time):
            pass
        else:
            print('You must make sure the initial purge time is the '
                  'same for all channels!')
        #print(self.purge_initial_time)
        print(self.purge_initial_pressure)
        #print(self.purge_continuous_pressure)

        # Idea: When this button is pressed do the inital purge (with
        # after and freezing the interface for that duration
        #
        self.child = tk.Toplevel() # this is used to freeze the
        self.child.grab_set() # original window
        self.child.wm_title('Please wait until purge is complete')

        # Put the purging code here
        # end with setting the channels to the continous_pressure
        for counter, i_channel in enumerate(self.purge_channels):
            self.update_OB1_channels(
                int(i_channel.split('Channel ')[-1]),
                self.purge_initial_pressure[counter])
        # !!!! The inital step time MUST be identical. Otherwise the
        # code will just take the longest time!
        time_to_wait = int(round(max(
            self.purge_initial_time) * 1000)) # need miliseconds!

        self.after(time_to_wait, self.child.destroy())

        print('Purging finished')

        # Set to continous purge values
        for counter, i_channel in enumerate(self.purge_channels):
            self.update_OB1_channels(
                int(i_channel.split('Channel ')[-1]),
                self.purge_continuous_pressure[counter])

        self.purge_initiated = True

    def update_OB1_channels(self, channel, value):
        """
        This should be a heavily used function.
        Do this on the epyhs PC
        :return:
        """
        if channel == 1 or channel == 2:
            if MIN_PRESS <= value <= MAX_PRESS12:
                error = ESI.OB1_Set_Press(Instr_ID.value,
                                          ctypes.c_int32(channel),  # convert to c_int32,
                                          ctypes.c_double(value),
                                          ctypes.byref(Calib),
                                          1000)
            else:
                print('CHANNEL 1 & 2 CAN ONLY TAKE ' + repr(MIN_PRESS) + ' to ' + repr(MAX_PRESS12) + '.'
                       '\nYou entered ' + repr(value) + ' for Channel ' + repr(channel))
        elif channel == 3 or channel == 4:
            if MIN_PRESS <= value <= MAX_PRESS34:
                error = ESI.OB1_Set_Press(Instr_ID.value,
                                          ctypes.c_int32(channel),  # convert to c_int32,
                                          ctypes.c_double(value),
                                          ctypes.byref(Calib),
                                          1000)
            else:
                print('CHANNEL 3 & 4 CAN ONLY TAKE ' + repr(MIN_PRESS) + ' to ' + repr(MAX_PRESS34) + '.'
                      '\nYou entered ' + repr(value) + ' for Channel ' + repr(channel))
        print('updating pressure on Ch' + repr(channel) + ' to value: ' + repr(value))

    def read_OB1_pressure(self, channel):
        """
        Does what it says
        :param channel:
        :return:
        """
        get_pressure = ctypes.c_double()

        error = ESI.OB1_Get_Press(Instr_ID.value,
                                  ctypes.c_int32(channel),
                                  1,
                                  ctypes.byref(Calib),
                                  ctypes.byref(get_pressure),
                                  1000)  # Acquire_data=1 -> read all the analog values
        return(get_pressure.value)

    def load_func(self):
        self.stim_file_path = filedialog.askopenfile(title='Select Stimulus File',
                                            initialdir=Path(Path.cwd(), 'stim_files')) #
        # fix initialdir to folder once on the ephys PC

        self.stim_file_name.set(Path(self.stim_file_path.name).name)

        self.stimulation_file = pd.read_csv(Path(
            self.stim_file_path.name), usecols=range(1,5))

        self.stim_duration = int(round(self.stimulation_file.shape[0]/FREQUENCY))

        time = np.linspace(0,
                         self.stimulation_file.shape[0]/FREQUENCY,
                         self.stimulation_file.shape[0])
        self.ax.clear()
        for columns in self.stimulation_file.columns:
            self.ax.plot(time, self.stimulation_file[columns],
                         label=columns)
        self.ax.set_ylabel('Pressure [mbar]')
        self.ax.set_xlabel('Time[s]')
        self.ax.legend()

        self.fig.tight_layout()

        self.canvas.draw()

    def present_stimulus_func(self):

        if self.purge_initiated:

            # Collect time for saving.
            now = datetime.datetime.now()
            real_pressure = np.zeros((self.stimulation_file.shape[0], 5))# I'm doing 5 for the 4 channels because
            # numpy starts indexing at 0 while OB1 starts at 1. By just ignoring 0
            # I make my live a bit easier

            # now everything is prepared. run a while loop that waits for the
            # TTL IN of OB1 to be set to HIGH using the Digidata
            print('waiting for trigger')
            waiting_for_trigger = True
            trigger_ext = ctypes.c_int32()
            while waiting_for_trigger:
                error = ESI.OB1_Get_Trig(Instr_ID, ctypes.byref(trigger_ext))
                if trigger_ext.value == 1:
                    waiting_for_trigger = False
            print('starting odor stimulus')

            # Need to keep track of time
            wait_time = 1 / FREQUENCY
            delays = []
            theoretical_time_passed = 0
            preciser_time = []

            # Keep the current pressure in an array to decrease amount of updating OB1
            current_pressure = np.zeros(5) # I'm doing 5 for the 4 channels because
            # numpy starts indexing at 0 while OB1 starts at 1. By just ignoring 0
            # I make my live a bit easier
            ########################################################
            # STIM LOOP START
            start_time = time.time()
            for i in range(int(self.stim_duration*FREQUENCY)):
                # TIC-TOC: Indicate each loop in Digidata with a brief TTL out
                # signal
                # Idea - set a a Trigger HIGH signal every tick to the Digidata.
                # Shouldn't be a problem for the Digidata and I can easily correlate the
                # two machines after than.
                error = ESI.OB1_Set_Trig(Instr_ID, ctypes.c_int32(True))
                # And turn off again - check if this is long enough for Digidata
                # to detect it (have 1ms resolution)
                error = ESI.OB1_Set_Trig(Instr_ID, ctypes.c_int32(False))

                # for loop to go through all the channels
                for counter, i_chan in enumerate(self.stimulation_file.columns):
                    desired_channel = int(i_chan.split('Ch')[-1])
                    desired_pressure = self.stimulation_file[i_chan][i]
                    # check if value is smaller than the continuous pressure defined in the
                    # purge.json file
                    if desired_channel == 1:
                        if self.purge_continuous_pressure[0] > desired_pressure:
                            desired_pressure = self.purge_continuous_pressure[0]
                    elif desired_channel == 2:
                        if self.purge_continuous_pressure[1] > desired_pressure:
                            desired_pressure = self.purge_continuous_pressure[1]
                    elif desired_channel == 3:
                        if self.purge_continuous_pressure[2] > desired_pressure:
                            desired_pressure = self.purge_continuous_pressure[2]
                    elif desired_channel == 4:
                        if self.purge_continuous_pressure[3] > desired_pressure:
                            desired_pressure = self.purge_continuous_pressure[3]

                    # Only update if different to previously set value! I'm sure
                    # it costs computing power to always update OB1 through USB so
                    # I'm trying to minimize this.
                    if current_pressure[desired_channel] != desired_pressure:
                        self.update_OB1_channels(channel=desired_channel,
                                                 value=desired_pressure)

                    current_pressure[desired_channel] = desired_pressure

                    # Equally important: Read the actually delivered pressure
                    real_pressure[i, desired_channel] = self.read_OB1_pressure(desired_channel)

                ####
                # Code below seems to work well
                # Tested for 100seconds and was only 20ms off.
                # This is of course incorrect for the first loop but I think for
                # my purposes this should be ok. I just need to make sure I
                delays.append(time.time() - start_time)
                # this seems to be necessary to get rid of accumulated error
                # which resulted in 10s of ms difference in a 10s loop at my home
                # machine
                preciser_time.append(theoretical_time_passed - delays[-1])

                theoretical_time_passed += wait_time
                if preciser_time[-1] > 0:  # don't pass negative time, just skip
                    # sleep
                    #time.sleep(preciser_time[-1])
                    self.after(int(round(preciser_time[-1]*1000)))

            # Set to continous purge values
            for counter, i_channel in enumerate(self.purge_channels):
                self.update_OB1_channels(
                    int(i_channel.split('Channel ')[-1]),
                    self.purge_continuous_pressure[counter])

            ###############################
            # Save the stimulation data
            # First, create a folder for this particular experiment
            folder_name = Path(self.save_directory.get(), now.strftime("%Y%m%d_%H-%M-%S_") + self.odor_var.get())
            folder_name.mkdir(parents=True, exist_ok=True)

            np.save(Path(folder_name, now.strftime("%Y%m%d_%H-%M-%S_") +
                        self.odor_var.get() + '_delays.npy'), delays)

            pd_presented_stim = pd.DataFrame()
            pd_presented_stim['Time'] = delays

            pd_presented_stim['Ch1[mbar]_Target'] = self.stimulation_file['Ch1']
            pd_presented_stim['Ch1[mbar]_Real'] = real_pressure[:, 1]

            pd_presented_stim['Ch2[mbar]_Target'] =  self.stimulation_file['Ch2']
            pd_presented_stim['Ch2[mbar]_Real'] = real_pressure[:, 2]

            pd_presented_stim['Ch3[mbar]_Target'] =  self.stimulation_file['Ch3']
            pd_presented_stim['Ch3[mbar]_Real'] = real_pressure[:, 3]

            pd_presented_stim['Ch4[mbar]_Target'] =  self.stimulation_file['Ch4']
            pd_presented_stim['Ch4[mbar]_Real'] = real_pressure[:, 4]

            pd_presented_stim.to_csv(Path(folder_name, now.strftime("%Y%m%d_%H-%M-%S_") +
                          self.odor_var.get() + '_pressure.csv'))

            fig = Figure()
            ax = fig.add_subplot(111)
            ax.plot(real_pressure[:, 1], label='Ch1')
            ax.plot(real_pressure[:, 2], label='Ch2')
            ax.plot(real_pressure[:, 3], label='Ch3')
            ax.plot(real_pressure[:, 4], label='Ch4')
            fig.legend()

            fig.savefig(Path(folder_name, now.strftime("%Y%m%d_%H-%M-%S_") +
                             self.odor_var.get() + '_pressure.png'))

        else:
            print('Please purge before running the experiment')

    def quit_func(self):
        """
        Exit the program
        """

        # set all channels to 0
        self.update_OB1_channels(1, MIN_PRESS)
        self.update_OB1_channels(2, MIN_PRESS)
        self.update_OB1_channels(3, MIN_PRESS)
        self.update_OB1_channels(4, MIN_PRESS)

        self.destroy()
        self.quit()


if __name__ == "__main__":
    root = tk.Tk()
    OB1_GUI(root)#.pack(side="top", fill="both", expand=True)
    root.mainloop()