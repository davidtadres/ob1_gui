# MAKE SURE ALL CHANNELS ARE CLOSED WITH ADEQUATE CAPS


doublecheck = input('Please confirm that all channels are closed as stated in the manual. '
                    'Enter "Yes" to confirm an calibrate the instrument')

if doublecheck == 'Yes':
    import ctypes
    import Elveflow64 as ESI
    from pathlib import Path

    Instr_ID=ctypes.c_int32()
    # print("Instrument name and regulator types are hardcoded in the Python script")
    # see User Guide to determine regulator types and NIMAX to determine the instrument name
    error=ESI.OB1_Initialization('0175C507'.encode('ascii'),1,1,2,2,
                              ctypes.byref(Instr_ID))

    #
    #Set the calibration
    #

    Calib=(ctypes.c_double*1000)()#always define array this way, calibration should have 1000 elements
    repeat=True

    Calib_path = str(Path(Path.cwd(), 'Calib.txt'))
    ####################
    while repeat:
        ESI.OB1_Calib(Instr_ID.value, Calib, 1000)
        # for i in range (0,1000):
        # print('[',i,']: ',Calib[i])
        error = ESI.Elveflow_Calibration_Save(Calib_path.encode('ascii'),
                                              ctypes.byref(Calib), 1000)
        print('calib saved in %s' % Calib_path.encode('ascii'))
        repeat = False


