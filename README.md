# OB1_GUI

This repository contains the graphical user interface to control the 
'Ob1' pressure controller from Elveflow. It was put together by David
Tadres while working in the Louis Lab.

The files in the folder 'DLL64' are provided by Elveflow as well 
as the script 'Elveflow64.py'. The newest version of that software
can be found here: https://www.elveflow.com/microfluidic-products/microfluidics-software/elveflow-software-sdk/

The relevant script is called 'GUI.py'. It was designed to work with 
our 'old' OB1 pressure controller. The pressure controller has two low
pressure channels (1 and 2 @ 200 mbar) and two high pressure channels 
(3 and 4 @ 2 bar). 

To get this interface to work with another OB1, it is necessary to:

1) Check user guide to understand how to get the instrument name. Here 
   it is '0175C507'.

2) Which channels are present (here 1, 1, 2, 2)

3) Add this information in:

   `ESI.OB1_Initialization(OB1_Initialization('0175C507'.encode('ascii'),1,1,2,2,ctypes.byref(Instr_ID))`

4) Then run 'GUI.py' using python.

Once the GUI opens, one first has to press 'purge' which will apply the
indicated pressure for the indicated amount of time. Finally, the 
indicated pressure (e.g. 5 mbar) will be constantly applied. This was 
necessary as having no pressure between experiments led to large delays
of odor delivery due to the capillary effect.

Once a stimulus file has been selected, the protocol is displayed in 
the GUI.

![GUI screenshot](https://gitlab.com/davidtadres/ob1_gui/-/blob/main/images/GUI.png)

When the user presses start, OB1 will wait until the 'TTL IN' receives
a trigger signal. Then the script presents the selected stimulus.

As a control, the script also instructs OB1 to fire 'TTL OUT' in each 
iteration. This helps the experimenter to keep track of lag.

Another script, 'GUI_and_video.py' integrates video recording using a 
PointGrey camera. This is most likely useless if not exactly the same 
camera is being used. 

# Installation

1. Install either Miniconda (https://conda.io/miniconda.html) or
Anaconda (https://www.anaconda.com/download/).
2. Install Git (https://git-scm.com/downloads) (already installed on
many Linux distributions!)

3. Open Terminal(Mac/Linux) or Command Prompt(Windows)

4. Create a new environment using conda:

    `conda create -n OB1_environment python=3.7`

5. Activate the environment in Windows by typing:

    ```activate OB1_environment```

    on Mac/Linux type:

    ```source activate OB1_environment```

6. Install necessary packages by typing all of the below:

    `conda install pandas -y`

    `conda install matplotlib -y`

    `conda install -c anaconda tk -y`
    
    `conda install scipy -y`
