import PyCapture2
import time
import numpy as np
from pathlib import Path

no_of_images = 100
save_path = 'foo'
video_name = 'test.avi'
brightness_value = 0
autoexposure_value = False
shutter_speed_value = 18
gain_value = 6
framerate_value = 10

######################################################################

def print_camera_info(cam):
    cam_info = cam.getCameraInfo()
    print('\n*** CAMERA INFORMATION ***\n')
    print('Serial number - %d' % cam_info.serialNumber)
    print('Camera model - %s' % cam_info.modelName)
    print('Camera vendor - %s' % cam_info.vendorName)
    print('Sensor - %s' % cam_info.sensorInfo)
    print('Resolution - %s' % cam_info.sensorResolution)
    print('Firmware version - %s' % cam_info.firmwareVersion)
    print('Firmware build time - %s' % cam_info.firmwareBuildTime)
    print()

#####################################################################
# Initialize Camera
# Ensure sufficient cameras are found
bus = PyCapture2.BusManager()
num_cams = bus.getNumOfCameras()
print('Number of cameras detected: ', num_cams)
if not num_cams:
    print('Insufficient number of cameras. Exiting...')
    exit()

cam = PyCapture2.Camera()
uid = bus.getCameraFromIndex(0)
cam.connect(uid)
print_camera_info(cam)

######################################################################
# config of the camera
# Missing: Option to do binning. Currently we're loosing a lot of FOV
# as we're just focusing on the center 1/4 of the video
cam.setConfiguration(grabMode = PyCapture2.GRAB_MODE.BUFFER_FRAMES)
# alternatively I can use 'DROP_FRAMES'
fmt7info, supported = cam.getFormat7Info(0)

fraction = 2 # I'm not free in choosing the pixels
desired_x = int(round(fmt7info.maxWidth/fraction))# fmt7info.maxWidth
desired_y = int(round(fmt7info.maxWidth/fraction)) # fmt7info.maxHeight
offset_x = fmt7info.maxWidth / 2 - (fmt7info.maxWidth/(fraction*2))
offset_y = fmt7info.maxWidth / 2 - (fmt7info.maxWidth/(fraction*2))

fmt7imgSet = PyCapture2.Format7ImageSettings(mode=0,
                                             offsetX=offset_x,
                                             offsetY=offset_x,
                                             width=desired_x,
                                             height=desired_y,
                                             pixelFormat=PyCapture2.PIXEL_FORMAT.MONO8)
fmt7pktInf, isValid = cam.validateFormat7Settings(fmt7imgSet)
if not isValid:
    raise RuntimeError("Format7 settings are not valid!")

cam.setFormat7ConfigurationPacket(fmt7pktInf.recommendedBytesPerPacket, fmt7imgSet)

fmt7info, supported = cam.getFormat7Info(0)

#print(fmt7info.maxWidth)
#print(supported)

# No need to use the trigger modes: I'll just call this function when
# OB1 gets triggered. Since everything's on the same clock/PC this
# shouldn't be a problem.
#cam.setTriggerMode(onOff = False,
#                   polarity = 1,
#                   source = 3,
#                   mode = 0)

######################################################################
def set_brightness(brightness):
    cam.setProperty(type = PyCapture2.PROPERTY_TYPE.BRIGHTNESS,
                    autoManualMode = False,
                    absControl=True, absValue = brightness)
def get_brightness():
    return(cam.getProperty(PyCapture2.PROPERTY_TYPE.BRIGHTNESS).absValue)

def set_shutter_speed(shutter):
    # It seems this is the way to fix the different properties
    cam.setProperty(type = PyCapture2.PROPERTY_TYPE.SHUTTER,
                    autoManualMode = False, absControl=True,
                    absValue = shutter)
def get_shutter_speed():
    return(cam.getProperty(PyCapture2.PROPERTY_TYPE.SHUTTER).absValue)

def set_autoexposure(autoexposure):
    """

    :param autoexposure: Bool
    :return:
    """
    cam.setProperty(type = PyCapture2.PROPERTY_TYPE.AUTO_EXPOSURE,
                    autoManualMode = autoexposure)
def get_autoexposure():
    return (cam.getProperty(PyCapture2.PROPERTY_TYPE.AUTO_EXPOSURE).autoManualMode)

def set_gamma(gamma):
    cam.setProperty(type = PyCapture2.PROPERTY_TYPE.GAMMA,
                    autoManualMode = False,
                    absControl=True, absValue = gamma)
def get_gamma():
    return(cam.getProperty(PyCapture2.PROPERTY_TYPE.GAMMA).absValue)

def set_gain(gain):
    cam.setProperty(type = PyCapture2.PROPERTY_TYPE.GAIN,
                    autoManualMode = False,
                    absControl=True, absValue = gain)
def get_gain():
    return(cam.getProperty(PyCapture2.PROPERTY_TYPE.GAIN).absValue)

def set_framerate(fps):
    cam.setProperty(type = PyCapture2.PROPERTY_TYPE.FRAME_RATE,
                    autoManualMode = False,
                    absControl=True, absValue = fps)
def get_framerate():
    return(cam.getProperty(PyCapture2.PROPERTY_TYPE.FRAME_RATE).absValue)

#cam.setProperty(type = PyCapture2.PROPERTY_TYPE.SHARPNESS,
#                autoManualMode = False,
#                absControl=True, absValue = 2.0)
#cam.setProperty(type = PyCapture2.PROPERTY_TYPE.WHITE_BALANCE,absValue = 2.0)

#print(get_autoexposure())
#print(get_shutter_speed())
#set_autoexposure(False)
#time.sleep(1)
#print(get_framerate())
#print(get_autoexposure())

####################################################################
# set cam settings
set_brightness(brightness_value)
set_autoexposure(autoexposure_value)
set_shutter_speed(shutter_speed_value)
set_gain(gain_value)
print(get_framerate())
set_framerate(framerate_value)
print(get_framerate())

######################################################################
# This should only be started when OB1 is triggered.
def capture_video():
    print('Starting capture...')
    video = PyCapture2.FlyCapture2Video()
    video.AVIOpen(video_name.encode('utf-8'), get_framerate())
    timestamps = np.zeros((no_of_images))

    #input('press enter to start')
    cam.startCapture()
    print('fps ' + repr(get_framerate()))

    start_time = time.time()
    for i in range(no_of_images):
        print(i)
        timestamps[i] = time.time() - start_time

        try:
            image = cam.retrieveBuffer()
            video.append(image)
        except PyCapture2.Fc2error as fc2Err:
            print('Error retrieving buffer : %s' % fc2Err)
            continue

    print('Time: ' + repr(time.time()-start_time))

    video.close()
    cam.stopCapture()

    np.save('../timestamps.npy', timestamps)
    print('Done with capture')

capture_video()

cam.disconnect()