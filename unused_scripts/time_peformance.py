# On my home PC I get at:
#   10 Hz a relatively uniform distribution from 100ms to 101ms
#   100 Hz an inverted gaussian with a peak at 10ms and another at 11ms
# -> about 1 ms off

import time
import datetime
import sys
from pathlib import Path
import ctypes
import Elveflow64 as ESI
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from tkinter import filedialog, Tk

update_frequency = 10 # test
#stimulation_duration = 100 # in seconds


root=Tk()
root.withdraw()
stimfile_path = filedialog.askopenfile(title='Select Stimulus File',
                                       initialdir=Path(Path.cwd(), '../stim_files')).name
stim = pd.read_csv(Path(stimfile_path), usecols=range(1,5))

stimulation_duration = int(round(stim.shape[0]/update_frequency))

odor = input('What odor will you present?')

# Choose a path to save the output file
save_path = filedialog.askdirectory(title='Select Folder to save stimulus file',
                                    initialdir=Path('D:\\'))

# Collect time for saving.
now = datetime.datetime.now()

sys.path.append(Path(str(Path.cwd()), '../DLL64'))#add the path of the library here
sys.path.append(Path(str(Path.cwd()), '../Python_64'))#add the path of the LoadElveflow.py
MIN_PRESS = 0
MAX_PRESS12 = 200 # This is for safety: Our OB1 can only deliver 200mbar in CH1 and CH2.
MAX_PRESS34 = 2000
#
# Initialization of OB1 ( ! ! ! REMEMBER TO USE .encode('ascii') ! ! ! )
#
Instr_ID=ctypes.c_int32()
print("Instrument name and regulator types are hardcoded in the Python script")
#see User Guide to determine regulator types and NIMAX to determine the instrument name
error=ESI.OB1_Initialization('0175C507'.encode('ascii'),1,1,2,2,
                          ctypes.byref(Instr_ID))
#all functions will return error codes to help you to debug your code, for further information refer to User Guide
print('error:%d' % error)
print("OB1 ID: %d" % Instr_ID.value)

####################
# Calibration
Calib = (ctypes.c_double * 1000)()  # always define array this way, calibration should have 1000 elements

Calib_path = str(Path(Path.cwd(), '../Calib.txt'))
error = ESI.Elveflow_Calibration_Load(Calib_path.encode(
    'ascii'), ctypes.byref(Calib), 1000)
#error=ESI.Elveflow_Calibration_Default(ctypes.byref(Calib),1000)
# for i in range (0,1000):
# print('[',i,']: ',Calib[i])
# repeat = False

####################

#####
# Channels
# removed channel 3 for now due to the strange bug I've
# been seeing. Don't want to have it at 2bar all the time!
channels = [1,2,3,4]
channel_names = ['Ch1', 'Ch2', 'Ch3', 'Ch4']

# Delivered stimulus
#presented_stim = pd.DataFrame()
real_pressure = np.zeros((stim.shape[0], len(channels)))

# Currently desired pressure
current_pressure = np.zeros((4))

wait_time = 1/update_frequency

get_pressure = ctypes.c_double()

delays = []
theoretical_time_passed = 0
preciser_time = []

# now everything is prepared. run a while loop that waits for the
# TTL IN of OB1 to be set to HIGH using the Digidata
print('waiting for trigger')
waiting_for_trigger = True
trigger_ext = ctypes.c_int32()
while waiting_for_trigger:
    error = ESI.OB1_Get_Trig(Instr_ID, ctypes.byref(trigger_ext))
    if trigger_ext.value==1:
        waiting_for_trigger=False
    #print('waiting')

print('starting odor stimulus')
start_time = time.time()
for i in range(int(update_frequency*stimulation_duration)):

    # TIC-TOC: Indicate each loop in Digidata with a brief TTL out
    # signal
    # Idea - set a a Trigger HIGH signal every tick to the Digidata.
    # Shouldn't be a problem for it and I can easily correlate the
    # two machines after than.
    error = ESI.OB1_Set_Trig(Instr_ID, ctypes.c_int32(True))
    # And turn off again - check if this is long enough for Digidata
    # to detect it (have 1ms resolution)
    error = ESI.OB1_Set_Trig(Instr_ID, ctypes.c_int32(False))

    ####################
    # Stim files are csv with 4 columns: 'Ch1', 'Ch2', 'Ch3' and 'Ch4'
    # It is important that the index fits with the desired frequency!
    for i_chan in range(len(channels)):
        desired_pressure = stim[channel_names[i_chan]][i]
        # only update if save to do so
        if channel_names[i_chan] == 'Ch3' or channel_names[i_chan]=='Ch4':
            if MIN_PRESS <= desired_pressure <= MAX_PRESS34:
                # only update if desired pressure different to what was set before
                if stim[channel_names[i_chan]][i] != current_pressure[i_chan]:
                    error = ESI.OB1_Set_Press(Instr_ID.value,
                                              ctypes.c_int32(channels[i_chan]), #convert to c_int32,
                                              ctypes.c_double(desired_pressure),
                                              ctypes.byref(Calib),
                                              1000)
            else:
                print('SAVE PRESSURE RANGE EXCEEDED - ONLY USE 0-2000mbar!!')
        else:
            if MIN_PRESS <= desired_pressure <= MAX_PRESS12:
                # only update if desired pressure different to what was set before
                if stim[channel_names[i_chan]][i] != current_pressure[i_chan]:
                    error = ESI.OB1_Set_Press(Instr_ID.value,
                                              ctypes.c_int32(channels[i_chan]), #convert to c_int32,
                                              ctypes.c_double(desired_pressure),
                                              ctypes.byref(Calib),
                                              1000)
            else:
                print('SAVE PRESSURE RANGE EXCEEDED - ONLY USE 0-200mbar!!')

        current_pressure[i_chan] = stim[channel_names[i_chan]][i]

        # Equally important: Read the actually delivered pressure
        error = ESI.OB1_Get_Press(Instr_ID.value,
                                  ctypes.c_int32(channels[i_chan]),
                                  1,
                                  ctypes.byref(Calib),
                                  ctypes.byref(get_pressure),
                                  1000)  # Acquire_data=1 -> read all the analog values
        real_pressure[i,i_chan] = get_pressure.value

    ####
    # Code below seems to work well
    # Tested for 100seconds and was only 20ms off.
    # This is of course incorrect for the first loop but I think for
    # my purposes this should be ok. I just need to make sure I
    delays.append(time.time() - start_time)
    # this seems to be necessary to get rid of accumulated error
    # which resulted in 10s of ms difference in a 10s loop at my home
    # machine
    preciser_time.append(theoretical_time_passed - delays[-1])

    theoretical_time_passed += wait_time
    if preciser_time[-1] > 0: # don't pass negative time, just skip
        # sleep
        time.sleep(preciser_time[-1])

# TURN ALL CHANNELS OFF
for i_chan in range(stim.shape[1]):
    error = ESI.OB1_Set_Press(Instr_ID.value, ctypes.c_int32(i_chan+1),
                              ctypes.c_double(0), ctypes.byref(Calib), 1000)

error=ESI.OB1_Destructor(Instr_ID.value)

#fig =plt.figure()
#ax=fig.add_subplot(211)
#ax.hist(np.diff(np.array(delays)))
#ax2=fig.add_subplot(212)
#ax2.plot(delays)
#plt.show(block=True)

np.save(Path(save_path, now.strftime("%Y%m%d_%H-%M-%S_") + odor +'_delays.npy'), delays)

fig = plt.figure()
ax=fig.add_subplot(111)
ax.plot(real_pressure[:,0], label='Ch1')
ax.plot(real_pressure[:,1], label='Ch2')
ax.plot(real_pressure[:,2], label='Ch3')
ax.plot(real_pressure[:,3], label='Ch4')
ax.plot(real_pressure.sum(axis=1), c='k', label='Sum')
fig.legend()

fig.savefig(Path(save_path, now.strftime("%Y%m%d_%H-%M-%S_") + odor + '_pressure.png'))

pd_presented_stim = pd.DataFrame()

pd_presented_stim['Time'] = delays

pd_presented_stim['Ch1[mbar]_Target'] = stim['Ch1']
pd_presented_stim['Ch1[mbar]_Real'] = real_pressure[:,0]

pd_presented_stim['Ch2[mbar]_Target'] = stim['Ch2']
pd_presented_stim['Ch2[mbar]_Real'] = real_pressure[:,1]

pd_presented_stim['Ch3[mbar]_Target'] = stim['Ch3']
pd_presented_stim['Ch3[mbar]_Real'] = real_pressure[:,2]

pd_presented_stim['Ch4[mbar]_Target'] = stim['Ch4']
pd_presented_stim['Ch4[mbar]_Real'] = real_pressure[:,3]

pd_presented_stim.to_csv(Path(save_path, now.strftime("%Y%m%d_%H-%M-%S_")
                              + odor + '_pressure.csv'))