
import Elveflow64 as ESI
import ctypes

def calibration():
    """
    Doublecheck if this is indeed correct
    :return:
    """
    Calib=(ctypes.c_double*1000)()#always define array this way, calibration should have 1000 elements

    error=ESI.Elveflow_Calibration_Default(ctypes.byref(Calib),1000)
    #for i in range (0,1000):
        #print('[',i,']: ',Calib[i])
    repeat=False

